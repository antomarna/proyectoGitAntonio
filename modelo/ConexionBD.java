package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexionBD {

    /* Atributos **************************************************************/

    private Connection conn;
    private Statement st;

    /* Constructores **********************************************************/
    public ConexionBD() {
        conn = null;
        st = null;
    }

    /* Métodos getters & setters **********************************************/
    public Connection getConn() {
        return conn;
    }

    public Statement getSt() {
        return st;
    }

    /* Métodos ****************************************************************/
    private void crearTablas() throws Exception {
        String sql;
        try {
            sql = "CREATE TABLE IF NOT EXISTS Cursos ("
                    + "id INTEGER NOT NULL,"
                    + "titulo VARCHAR(30) NOT NULL,"
                    + "horas DECIMAL(6,2),"
                    + "fecIni DATE NULL,"
                    + "fecFin DATE NULL,"
                    + "modalidad CHAR(1) NOT NULL,"
                    + "estado VARCHAR(12)NULL,"
                    + "CONSTRAINT pk_Cursos PRIMARY KEY (id))";
            st.executeUpdate(sql);
            sql = "CREATE TABLE IF NOT EXISTS Alumnos ("
                    + "idCurso INTEGER NOT NULL,"
                    + "dni VARCHAR(10) NOT NULL,"
                    + "nombre VARCHAR(50) NOT NULL,"
                    + "mayoredad BOOLEAN NOT NULL,"
                    + "repetidor BOOLEAN NOT NULL,"
                    + "CONSTRAINT pk_Alumnos PRIMARY KEY (idCurso,dni),"
                    + "CONSTRAINT fk_Alumnos_Cursos FOREIGN KEY (idCurso)"
                    + " REFERENCES Cursos (id) ON DELETE CASCADE)";
//                        " REFERENCES Cursos (id))";
            st.executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error crearTablas()!!", e);
        }
    }

    public void abrirConexion() throws Exception {
        try {
            String url = "jdbc:mysql://localhost:3306/proyectogit?useSSL=false";
            conn = DriverManager.getConnection(url, "root", "alumno");
            st = conn.createStatement();
            crearTablas();
        } catch (SQLException e) {
            throw new Exception("Error abrirConexion()!!", e);
        }
    }

    public void cerrarConexion() throws Exception {
        try {
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            throw new Exception("Error cerrarConexion()!!", e);
        }
    }

}
